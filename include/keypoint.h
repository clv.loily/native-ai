//
// Created by VinhLoiIT on 5/21/2022.
//

#ifndef DEMO_KEYPOINT_H
#define DEMO_KEYPOINT_H

#include "tflite_model.h"
#include <vector>
#include "opencv2/core.hpp"


class KeypointModel : public TFLiteModel {
public:
    static constexpr const size_t INPUT_MODEL_WIDTH = 224;
    static constexpr const size_t INPUT_MODEL_HEIGHT = 224;
    static constexpr const float INPUT_MODEL_MEAN = 0.0f;
    static constexpr const float INPUT_MODEL_STD = 1.0f;
    static constexpr const size_t INPUT_MODEL_TENSOR_INDEX = 0;
    static constexpr const size_t OUTPUT_MODEL_TENSOR_INDEX = 0;
    static constexpr const size_t NUM_KEYPOINT = 5;

    static cv::Mat preprocessing(const cv::Mat& inputImage);
    std::vector<cv::Point2f> detectKeypoint(const cv::Mat& image) const;
};

#endif //DEMO_KEYPOINT_H
