//
// Created by VinhLoiIT on 5/21/2022.
//

#ifndef DEMO_DETCROPPOSE_H
#define DEMO_DETCROPPOSE_H

#include <memory>
#include "opencv2/core.hpp"
#include "yolov5.h"

class KeypointModel;

class YoloV5Model;

class DetCropPose {
private:
    static constexpr const float EXPAND_RATIO = 60.0f;
    std::shared_ptr<YoloV5Model> yoloModel;
    std::shared_ptr<KeypointModel> keypointModel;
public:
    DetCropPose(std::shared_ptr<YoloV5Model> yoloModel,
                std::shared_ptr<KeypointModel> keypointModel) : yoloModel{yoloModel},
                                                                keypointModel{keypointModel} {
    }

    std::vector<std::vector<cv::Point2f>>
    detectKeypoints(const cv::Mat &inputImage, std::vector<DetectionResult> const *boxes = nullptr) const;
    void release();
};

#endif //DEMO_DETCROPPOSE_H
