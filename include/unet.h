#pragma once

#include <opencv2/core/mat.hpp>
#include "tflite_model.h"

class UnetModel : public TFLiteModel {
public:
    static constexpr const size_t DOG_INDEX = 1;
    static cv::Mat preprocessing(const cv::Mat &image);
    static cv::Mat postprocessing(const cv::Mat &modelOutputImage, cv::Size outputSize,
                                  size_t numOutChannels = 1);
    cv::Mat extractAlpha(const cv::Mat &image, size_t numOutChannels = 1) const;
};