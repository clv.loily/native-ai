//
// Created by VinhLoiIT on 5/21/2022.
//

#ifndef DEMO_YOLOV5_H
#define DEMO_YOLOV5_H

#include <string>
#include <queue>
#include <vector>
#include "opencv2/core.hpp"
#include "tflite_model.h"


struct RectF {
    float left;
    float top;
    float right;
    float bottom;

    RectF(float l, float t, float r, float b) : left{l}, top{t}, right{r}, bottom{b} {}

    [[nodiscard]] float width() const {
        return std::abs(right - left);
    }

    [[nodiscard]] float height() const {
        return std::abs(bottom - top);
    }
};

struct DetectionResult {
    RectF rect;
    float boxConfident;
    size_t classId;
    float classConfident;

    DetectionResult(float x, float y, float w, float h, float boxConf, size_t classId,
                    float classConf) :
            rect{x, y, x + w, y + h}, boxConfident{boxConf}, classId{classId},
            classConfident{classConf} {}

    [[nodiscard]] float getConfident() const {
        return boxConfident;
    }

    bool operator<(const DetectionResult &other) const {
        return this->getConfident() < other.getConfident();
    }
};

float overlap(float x1, float w1, float x2, float w2);

float boxIntersection(RectF a, RectF b);

float boxUnion(RectF a, RectF b);

float boxIoU(RectF a, RectF b);

std::vector<DetectionResult>
parseModelOutput(const float *buffer, size_t numOutBox, size_t numClasses, float boxThreshold,
                 size_t modelImageWidth, size_t modelImageHeight);

std::vector<DetectionResult>
nonMaximumSuppression(const std::vector<DetectionResult> &boxes, size_t numClasses, float nmsThreshold);

RectF rescaleBox(RectF box, cv::Size modelInputSize, cv::Size orgImageSize);
cv::Mat resizeWithCropOrPad(cv::Mat inputImage, cv::Size targetSize);

class YoloV5Model : public TFLiteModel
{
private:
public:
    static constexpr const size_t INPUT_MODEL_WIDTH = 224;
    static constexpr const size_t INPUT_MODEL_HEIGHT = 224;
    static constexpr const float INPUT_MODEL_MEAN = 0.0f;
    static constexpr const float INPUT_MODEL_STD = 255.0f;
    static constexpr const size_t INPUT_MODEL_TENSOR_INDEX = 0;
    static constexpr const size_t OUTPUT_MODEL_TENSOR_INDEX = 0;
    static constexpr const size_t NUM_CLASSES = 1;
    static constexpr const size_t NUM_OUT_BOX = 3087;
    static constexpr const float BOX_THRESHOLD = 0.4f;
    static constexpr const float NMS_THRESHOLD = 0.45f;
    static constexpr const size_t DOG_INDEX = 0;
    cv::Mat preprocessing(cv::Mat inputImage, cv::Size cropSize) const;
    std::vector<DetectionResult> detectBoxes(cv::Mat inputImage) const;

};

#endif //DEMO_YOLOV5_H
