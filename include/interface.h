#pragma once

#include "tflite_model.h"


extern "C"
{

DLLExport int initUnetModelFromBuffer(const void *buffer, size_t bufferSize, bool useGPU);
DLLExport int initUnetModelFromPath(const char *modelPath, bool useGPU);
DLLExport void releaseUnetModel();
DLLExport void
extractAlpha(const void *rgbaData, size_t width, size_t height, size_t numInChannels,
             uint8_t *outData, size_t numOutChannels = 1, bool vFlipInput = false,
             bool hFlipInput = false);

// Interfaces for Keypoints
DLLExport int initKeypointModelFromBuffer(const void *yoloBuffer, size_t yoloBufferSize,
                                          const void *keypointBuffer, size_t keypointBufferSize, bool useGPU);
DLLExport int initKeypointModelFromPath(const char *yoloModelPath, const char *keypointModelPath, bool useGPU);
DLLExport void releaseKeypointModel();
DLLExport size_t
detectKeypoint(const void *rgbaData, size_t width, size_t height, size_t numInChannels,
               void *outBuffer, size_t bufferSize, bool vFlipInput = false,
               bool hFlipInput = false);
}
