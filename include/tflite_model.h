#pragma once

#include <memory>
#include <string>
#include "tensorflow/lite/interpreter.h"
#include "tensorflow/lite/model_builder.h"
#include "tensorflow/lite/delegates/gpu/delegate.h"

#ifdef WIN32
#define DLLExport __declspec(dllexport)
#elif ANDROID
#include "jni.h"
#define DLLExport JNIEXPORT
#else
#define DLLExport
#endif


enum ErrCode {
    OK = 0,
    INIT_LOAD,
    INIT_INTERPRETER,
    INIT_GPU,
    INIT_ALLOCATE,
};


class TFLiteModel {
protected:
    std::unique_ptr<tflite::FlatBufferModel> model;
    std::unique_ptr<tflite::Interpreter> interpreter;
    TfLiteDelegate* gpuDelegate = nullptr;

    ErrCode load(bool useGPU = false);
    ErrCode allocateTensors();
    ErrCode createInterpreter();
    void setDefaultOptions();

public:
    ~TFLiteModel();

    ErrCode loadModelFromPath(std::string_view modelPath, bool useGPU);

    ErrCode loadModelFromBuffer(const void *buffer, size_t bufferSize, bool useGPU);

    void release();

    static std::string logModelInfo(std::unique_ptr<tflite::Interpreter> interpreter);
};
