# Android Native AI Library

## Prerequisite

## Build

Let say we have
```
NINJA_PATH=C:\Users\VinhLoiIT\AppData\Local\Android\Sdk\cmake\3.18.1\bin\ninja.exe
ANDROID_NDK=C:\Users\VinhLoiIT\AppData\Local\Android\Sdk\ndk\21.4.7075529
```

The template building command is
```
mkdir build
cd build
cmake -DBUILD_SHARED_LIBS=1 -DCMAKE_TOOLCHAIN_FILE=${ANDROID_NDK}\build\cmake\android.toolchain.cmake -DCMAKE_MAKE_PROGRAM=${NINJA_PATH} -G Ninja -DCMAKE_BUILD_TYPE=Release -DANDROID_ABI=arm64-v8a -DANDROID_PLATFORM=android-30 -DANDROID_TOOLCHAIN=clang -DANDROID_STL=c++_shared -DANDROID_CPP_FEATURES=rtti ..
cmake --build .
```

Example building command
```
mkdir build
cd build
cmake -DBUILD_SHARED_LIBS=1 -DCMAKE_TOOLCHAIN_FILE=C:\Users\VinhLoiIT\AppData\Local\Android\Sdk\ndk\21.4.7075529\build\cmake\android.toolchain.cmake -DCMAKE_MAKE_PROGRAM=C:\Users\VinhLoiIT\AppData\Local\Android\Sdk\cmake\3.18.1\bin\ninja.exe -G Ninja -DCMAKE_BUILD_TYPE=Release -DANDROID_ABI=arm64-v8a -DANDROID_PLATFORM=android-30 -DANDROID_TOOLCHAIN=clang -DANDROID_STL=c++_shared -DANDROID_CPP_FEATURES=rtti ..
cmake --build .
```

Similarly, building command on MacOS (tested on Mac M1)
```
cmake -DBUILD_SHARED_LIBS=1 -DCMAKE_TOOLCHAIN_FILE=/Users/admin/Library/Android/sdk/ndk/21.4.7075529/build/cmake/android.toolchain.cmake -DCMAKE_MAKE_PROGRAM=/Users/admin/Library/Android/sdk/cmake/3.18.1/bin/ninja -G Ninja -DCMAKE_BUILD_TYPE=Release -DANDROID_ABI=arm64-v8a -DANDROID_PLATFORM=android-30 -DANDROID_TOOLCHAIN=clang -DANDROID_CPP_FEATURES=rtti ..
```

**Note**: We can change `-DANDROID_ABI=arm64-v8a` to different ABI to build

```
-DANDROID_ABI=arm64-v8a
-DANDROID_ABI=armeabi-v7a
-DANDROID_ABI=x86
-DANDROID_ABI=x86_64
```

Output libraries are placed at `build/lib`
