# Build with Docker instruction

## Installation

- Download Dockerfile: https://www.tensorflow.org/lite/android/lite_build
- References: https://blog.seeso.io/building-tensorflow-lite-c-in-android-1c8de1639e1d 

```bash
docker build -t tflite -f Dockerfile .
docker run -it -v $(pwd)/build:/host_dir tflite bash
```

**Inside docker container**

```bash
export ANDROID_API_LEVEL=31                 # smol-aos-ai use this version
export ANDROID_BUILD_TOOLS_VERSION=30.0.2   # unity use this version

sdkmanager \
  "build-tools;${ANDROID_BUILD_TOOLS_VERSION}" \
  "platform-tools" \
  "platforms;android-${ANDROID_API_LEVEL}"

rm -rf /android/sdk/platform-tools
mv /android/sdk/platform-tools-2 /android/sdk/platform-tools

# checkout tensorflow v2.8
cd /tensorflow_src
git fetch --all --tags
git checkout v2.8.2
```

```bash
# Configure
./configure

You have bazel 4.2.1 installed.
Please specify the location of python. [Default is /usr/bin/python3]:

Found possible Python library paths:
  /usr/lib/python3/dist-packages
  /usr/local/lib/python3.8/dist-packages
Please input the desired Python library path to use.  Default is [/usr/lib/python3/dist-packages]

Do you wish to build TensorFlow with ROCm support? [y/N]: N
No ROCm support will be enabled for TensorFlow.

Do you wish to build TensorFlow with CUDA support? [y/N]: N
No CUDA support will be enabled for TensorFlow.

Do you wish to download a fresh release of clang? (Experimental) [y/N]: N
Clang will not be downloaded.

Please specify optimization flags to use during compilation when bazel option "--config=opt" is specified [Default is -Wno-sign-compare]:


Would you like to interactively configure ./WORKSPACE for Android builds? [y/N]: y
Searching for NDK and SDK installations.

Preconfigured Bazel build configs. You can use any of the below by adding "--config=<>" to your build command. See .bazelrc for more details.
        --config=mkl            # Build with MKL support.
        --config=mkl_aarch64    # Build with oneDNN and Compute Library for the Arm Architecture (ACL).
        --config=monolithic     # Config for mostly static monolithic build.
        --config=numa           # Build with NUMA support.
        --config=dynamic_kernels        # (Experimental) Build kernels into separate shared objects.
        --config=v1             # Build with TensorFlow 1 API instead of TF 2 API.
Preconfigured Bazel build configs to DISABLE default on features:
        --config=nogcp          # Disable GCP support.
        --config=nonccl         # Disable NVIDIA NCCL support.
Configuration finished
```

```bash
# build armeabi-v7
bazel build -c opt --config=android_arm --cpu=armeabi-v7a //tensorflow/lite:libtensorflowlite.so
mkdir /host_dir/build-armeabi-v7a
cp -r bazel-bin/* /host_dir/build-armeabi-v7a

# clean bazel build before building another one
bazel clean

# build arm64
bazel build -c opt --config=android_arm64 --cpu=arm64-v8a //tensorflow/lite:libtensorflowlite.so
mkdir /host_dir/build-arm64-v8a
cp -r bazel-bin/* /host_dir/build-arm64-v8a

bazel build -c opt --config=android_arm64 --cpu=x86_64 //tensorflow/lite:libtensorflowlite.so
```

Similarly, to build GPU library:
```
bazel build -c opt --cxxopt=--std=c++17 --config=android_arm --cpu=armeabi-v7a tensorflow/lite/delegates/gpu:libtensorflowlite_gpu_delegate.so
```