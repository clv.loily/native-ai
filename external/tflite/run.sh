#!/bin/bash

CONFIGS="android_arm,armeabi-v7a android_arm64,arm64-v8a android_arm64,x86_64  android_arm64,x86"
TFLITE_SRC="tensorflow"
OUTPUT_DIR="$(pwd)/lib"


cd $TFLITE_SRC
echo $(pwd)
for config in $CONFIGS;
do
    bazel clean
    IFS=","
    set -- $config
    config=$1
    cpu=$2
    echo "Build tflite for --config=$config --cpu=$cpu"
    bazel build -c opt --config=$config --cpu=$cpu //tensorflow/lite:libtensorflowlite.so
    out_lib_dir="$OUTPUT_DIR/$cpu"
    mkdir -p $out_lib_dir
    cp bazel-bin/tensorflow/lite/libtensorflowlite.so $out_lib_dir
done

echo "Build GPU delegates..."

for config in $CONFIGS;
do
    bazel clean
    IFS=","
    set -- $config
    config=$1
    cpu=$2
    echo "Build tflite GPU delegate for --config=$config --cpu=$cpu"
    bazel build -c opt --cxxopt=--std=c++17 --config=$config --cpu=$cpu tensorflow/lite/delegates/gpu:libtensorflowlite_gpu_delegate.so
    out_lib_dir="$OUTPUT_DIR/$cpu"
    mkdir -p $out_lib_dir
    cp bazel-bin/tensorflow/lite/delegates/gpu/libtensorflowlite_gpu_delegate.so $out_lib_dir
done
