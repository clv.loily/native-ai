//
// Created by VinhLoiIT on 5/21/2022.
//

#include "detcroppose.h"
#include "yolov5.h"
#include "keypoint.h"


RectF getSquareRect(RectF inputRect) {
    float w = inputRect.width();
    float h = inputRect.height();
    float top = inputRect.top;
    float bottom = inputRect.bottom;
    float left = inputRect.left;
    float right = inputRect.right;
    if (w > h) {
        top -= (w - h) / 2;
        bottom += (w - h) / 2;
    } else if (h > w) {
        left -= (h - w) / 2;
        right += (h - w) / 2;
    }
    return RectF(left, top, right, bottom);
}

RectF getScaleRect(RectF rect, cv::Size imageSize) {
    constexpr const float EXPAND_RATIO = 60.0f;
    float minEdge = std::min((float) imageSize.height, (float) imageSize.width);
    float zoomValueW = EXPAND_RATIO * rect.width() / minEdge;
    float zoomValueH = EXPAND_RATIO * rect.height() / minEdge;
    if (rect.top - zoomValueH < 0 && rect.top >= 0) {
        zoomValueH = rect.top;
    }
    if (rect.left - zoomValueW < 0 && rect.left >= 0) {
        zoomValueW = rect.left;
    }
    if (rect.right + zoomValueW > imageSize.width && imageSize.width - rect.right >= 0 &&
        zoomValueW > imageSize.width - rect.right) {
        zoomValueW = imageSize.width - rect.right;
    }
    if (rect.bottom + zoomValueH > imageSize.height && imageSize.height - rect.bottom >= 0 &&
        zoomValueH > imageSize.height - rect.bottom) {
        zoomValueH = imageSize.height - rect.bottom;
    }

    return RectF(
            rect.left - zoomValueW,
            rect.top - zoomValueH,
            rect.right + zoomValueW,
            rect.bottom + zoomValueH
    );
}

RectF normalizeRect(RectF rect, cv::Size imageSize) {
    return RectF(
            std::max(0.0f, rect.left),
            std::max(0.0f, rect.top),
            std::min(imageSize.width - 1.0f, rect.right),
            std::min(imageSize.height - 1.0f, rect.bottom)
    );
}


RectF preprocessBox(RectF rect, cv::Size imageSize) {
    rect = getSquareRect(rect);
    rect = getScaleRect(rect, imageSize);
    return normalizeRect(rect, imageSize);
}


std::vector<std::vector<cv::Point2f>> DetCropPose::detectKeypoints(const cv::Mat &inputImage,
                                                                   std::vector<DetectionResult> const *boxesPtr) const {
    const std::vector<DetectionResult> &boxes = (boxesPtr == nullptr)
                                                ? this->yoloModel->detectBoxes(inputImage)
                                                : *boxesPtr;

    std::vector<std::vector<cv::Point2f>> outputs;
    cv::Size imageSize{inputImage.cols, inputImage.rows};
    for (const DetectionResult &box : boxes) {
        RectF normRect = preprocessBox(box.rect, imageSize);
        cv::Rect cropRect{static_cast<int>(normRect.left), static_cast<int>(normRect.top),
                          static_cast<int>(normRect.width()), static_cast<int>(normRect.height())};
        cv::Mat croppedImage = inputImage(cropRect);
        std::vector<cv::Point2f>&& boxKeypoint = keypointModel->detectKeypoint(croppedImage);
        for (cv::Point2f &p : boxKeypoint) {
            p.x += normRect.left;
            p.y += normRect.top;
        }
        outputs.push_back(boxKeypoint);
    }
    return outputs;
}

void DetCropPose::release() {
    yoloModel->release();
    keypointModel->release();
    yoloModel.reset();
    keypointModel.reset();
}


