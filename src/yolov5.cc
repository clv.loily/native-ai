//
// Created by VinhLoiIT on 5/21/2022.
//
#include <string>
#include <queue>
#include <vector>
#include <iostream>
#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "yolov5.h"


float overlap(float x1, float w1, float x2, float w2) {
    float l1 = x1 - w1 / 2;
    float l2 = x2 - w2 / 2;
    float left = (l1 > l2) ? l1 : l2;
    float r1 = x1 + w1 / 2;
    float r2 = x2 + w2 / 2;
    float right = (r1 < r2) ? r1 : r2;
    return right - left;
}

float boxIntersection(RectF a, RectF b) {
    float w = overlap(
            (a.left + a.right) / 2, (a.right - a.left),
            (b.left + b.right) / 2, (b.right - b.left)
    );
    float h = overlap(
            (a.top + a.bottom) / 2, (a.bottom - a.top),
            (b.top + b.bottom) / 2, (b.bottom - b.top)
    );
    if (w < 0 || h < 0) return 0.0f;
    return w * h;
}

float boxUnion(RectF a, RectF b) {
    float i = boxIntersection(a, b);
    return (a.right - a.left) * (a.bottom - a.top) + (b.right - b.left) * (b.bottom - b.top) - i;
}

float boxIoU(RectF a, RectF b) {
    return boxIntersection(a, b) / boxUnion(a, b);
}

std::vector<DetectionResult>
parseModelOutput(const float *buffer, size_t numOutBox, size_t numClasses, float boxThreshold,
                 size_t modelImageWidth, size_t modelImageHeight) {
    std::vector<DetectionResult> outputs;
    auto imageWidth = static_cast<float>(modelImageWidth);
    auto imageHeight = static_cast<float>(modelImageHeight);
    for (size_t index = 0; index < numOutBox; index++) {
        float boxConfidence = buffer[index * (numClasses + 5) + 4];
        float classConfidence = buffer[index * (numClasses + 5) + 5];
        float confidence = classConfidence * boxConfidence;
        if (confidence > boxThreshold) {
            float w = buffer[index * (numClasses + 5) + 2] * imageWidth;
            float h = buffer[index * (numClasses + 5) + 3] * imageHeight;
            float x = buffer[index * (numClasses + 5) + 0] * imageWidth - w / 2;
            float y = buffer[index * (numClasses + 5) + 1] * imageHeight - h / 2;
            DetectionResult result{x, y, w, h, boxConfidence, 0, classConfidence};
            outputs.push_back(result);
        }
    }
    return outputs;
}

std::vector<DetectionResult>
nonMaximumSuppression(const std::vector<DetectionResult> &boxes, size_t numClasses,
                      float nmsThreshold) {
    std::vector<DetectionResult> outputs;
    for (size_t classId = 0; classId < numClasses; classId++) {
        std::priority_queue<DetectionResult> queue;
        // 1. filter boxes by classes
        for (auto &box: boxes) {
            if (box.classId == classId) {
                queue.push(box);
            }
        }

        //2.do non maximum suppression
        while (!queue.empty()) {
            //insert detection with max confidence
            auto max = queue.top();
            outputs.push_back(max);
            queue.pop();

            std::priority_queue<DetectionResult> tmp;
            while (!queue.empty()) {
                auto &currentDetection = queue.top();
                queue.pop();
                if (boxIoU(max.rect, currentDetection.rect) < nmsThreshold) {
                    tmp.push(currentDetection);
                }
            }
            queue = tmp;
        }
    }
    return outputs;
}

RectF rescaleBox(RectF box, cv::Size modelInputSize, cv::Size orgImageSize) {
    float xFactor =
            static_cast<float>(orgImageSize.width) / static_cast<float>(modelInputSize.width);
    float yFactor =
            static_cast<float>(orgImageSize.height) / static_cast<float>(modelInputSize.height);

    float newX = box.left * xFactor;
    float newY = box.top * yFactor;
    float newWidth = box.width() * xFactor;
    float newHeight = box.height() * yFactor;

    return RectF{newX, newY, newX + newWidth, newY + newHeight};
}

cv::Point2f inverseCropPad(cv::Point2f point, cv::Size srcSize, cv::Size targetSize)
{
    float x = point.x + ((float)targetSize.width - (float)srcSize.width) / 2.0f;
    float y = point.y + ((float)targetSize.height - (float)srcSize.height) / 2.0f;
    return {x, y};
}

cv::Mat resizeWithCropOrPad(cv::Mat inputImage, cv::Size targetSize) {
    cv::Mat outputImage = cv::Mat::zeros(targetSize, inputImage.type());

    int w = inputImage.cols;
    int h = inputImage.rows;
    int srcL;
    int srcR;
    int dstL;
    int dstR;
    if (targetSize.width > w) {
        srcL = 0;
        srcR = w;
        dstL = (targetSize.width - w) / 2;
        dstR = dstL + w;
    } else {
        dstL = 0;
        dstR = targetSize.width;
        srcL = (w - targetSize.width) / 2;
        srcR = srcL + targetSize.width;
    }

    int srcT;
    int srcB;
    int dstT;
    int dstB;
    if (targetSize.height > h) {
        srcT = 0;
        srcB = h;
        dstT = (targetSize.height - h) / 2;
        dstB = dstT + h;
    } else {
        dstT = 0;
        dstB = targetSize.height;
        srcT = (h - targetSize.height) / 2;
        srcB = srcT + targetSize.height;
    }

    cv::Rect srcRect{srcL, srcT, std::abs(srcR - srcL), std::abs(srcT - srcB)};
    cv::Rect dstRect{dstL, dstT, std::abs(dstR - dstL), std::abs(dstT - dstB)};
    inputImage(srcRect).copyTo(outputImage(dstRect));

    return outputImage;
}

cv::Mat YoloV5Model::preprocessing(cv::Mat inputImage, cv::Size cropSize) const {
    cv::Mat cropOrPad = resizeWithCropOrPad(inputImage, cropSize);
    cv::Mat resized;
    cv::resize(cropOrPad, resized, cv::Size(INPUT_MODEL_WIDTH, INPUT_MODEL_HEIGHT));
    cv::Mat out;
    resized.convertTo(out, CV_32F, 1 / INPUT_MODEL_STD, -INPUT_MODEL_MEAN);
    return out;
}

std::vector<DetectionResult> YoloV5Model::detectBoxes(cv::Mat inputImage) const {
    int maxSize = std::max(inputImage.rows, inputImage.cols);
    cv::Size cropSize{maxSize, maxSize};
    cv::Mat modelInputImage = preprocessing(inputImage, cropSize);

    auto inputTensor = interpreter->input_tensor(INPUT_MODEL_TENSOR_INDEX);
    uint32_t totalInputBytes = modelInputImage.total() * modelInputImage.elemSize();

    auto inputBuffer = inputTensor->data.f;
    memcpy(inputBuffer, modelInputImage.reshape(1, 1).data, totalInputBytes);

    interpreter->Invoke();

    auto outputTensor = interpreter->output_tensor(OUTPUT_MODEL_TENSOR_INDEX);
    float *outputArray = outputTensor->data.f;

    std::vector<DetectionResult> boxes;
    for (size_t index = 0; index < NUM_OUT_BOX; index++) {
        float boxConfidence = outputArray[index * (NUM_CLASSES + 5) + 4];
        float classConfidence = outputArray[index * (NUM_CLASSES + 5) + 5];
        float confidence = classConfidence * boxConfidence;
        if (confidence > BOX_THRESHOLD) {
            float w = outputArray[index * (NUM_CLASSES + 5) + 2] * INPUT_MODEL_WIDTH;
            float h = outputArray[index * (NUM_CLASSES + 5) + 3] * INPUT_MODEL_HEIGHT;
            float x = outputArray[index * (NUM_CLASSES + 5) + 0] * INPUT_MODEL_WIDTH - w / 2;
            float y = outputArray[index * (NUM_CLASSES + 5) + 1] * INPUT_MODEL_HEIGHT - h / 2;

            DetectionResult result{
                    x, y, w, h,
                    boxConfidence, DOG_INDEX, classConfidence
            };
            boxes.push_back(result);
        }
    }

    boxes = nonMaximumSuppression(boxes, NUM_CLASSES, NMS_THRESHOLD);
    cv::Size modelSize = cv::Size(INPUT_MODEL_WIDTH, INPUT_MODEL_HEIGHT);
    for (auto &box : boxes) {
        std::cout << box.rect.left << ' ' << box.rect.top << ' ' << box.rect.right << ' ' << box.rect.bottom << std::endl;
        RectF rect = rescaleBox(box.rect, modelSize, cropSize);
        std::cout << rect.left << ' ' << rect.top << ' ' << rect.right << ' ' << rect.bottom << std::endl;
        cv::Point2f topLeft = inverseCropPad(cv::Point2f{rect.left, rect.top}, cropSize, cv::Size(inputImage.cols, inputImage.rows));
        cv::Point2f botRight = inverseCropPad(cv::Point2f{rect.right, rect.bottom}, cropSize, cv::Size(inputImage.cols, inputImage.rows));
        rect.left = topLeft.x;
        rect.top = topLeft.y;
        rect.right = botRight.x;
        rect.bottom = botRight.y;
        box.rect = rect;
    }

    return boxes;
}
