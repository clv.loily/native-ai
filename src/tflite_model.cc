#include <string>

#include "tflite_model.h"

#include "tensorflow/lite/model_builder.h"
#include "tensorflow/lite/kernels/register.h"
#include "tensorflow/lite/model.h"


ErrCode TFLiteModel::loadModelFromPath(std::string_view modelPath, bool useGPU) {
    model = tflite::FlatBufferModel::BuildFromFile(modelPath.data());
    if (model == nullptr) {
        return ErrCode::INIT_LOAD;
    }
    load(useGPU);
    return ErrCode::OK;
}

ErrCode TFLiteModel::loadModelFromBuffer(const void *buffer, size_t bufferSize, bool useGPU) {
    model = tflite::FlatBufferModel::BuildFromBuffer((const char *) buffer, bufferSize);
    if (model == nullptr) {
        return ErrCode::INIT_LOAD;
    }

    load(useGPU);
    return ErrCode::OK;
}

std::string TFLiteModel::logModelInfo(std::unique_ptr<tflite::Interpreter> interpreter) {
    std::ostringstream out;
    auto inputTensor = interpreter->input_tensor(0);
    out << "Input shape: " << inputTensor->dims->data[0];
    for (int i = 1; i < inputTensor->dims->size; i++)
        out << ',' << inputTensor->dims->data[i];
    out << std::endl;
    out << "Input total bytes: " << inputTensor->bytes << std::endl;
    out << "Input tensor data type: " << inputTensor->type << std::endl;

    auto outputTensor = interpreter->output_tensor(0);

    out << "Output shape: " << outputTensor->dims->data[0];
    for (int i = 0; i < outputTensor->dims->size; i++)
        out << ',' << outputTensor->dims->data[i];
    out << std::endl;
    out << "Output total bytes: " << outputTensor->bytes << std::endl;
    out << "Output tensor data type: " << outputTensor->type << std::endl;

    return out.str();
}

void TFLiteModel::release() {
    if (gpuDelegate != nullptr)
    {
        TfLiteGpuDelegateV2Delete(gpuDelegate);
        gpuDelegate = nullptr;
    }
    interpreter.reset();
    model.reset();
}

ErrCode TFLiteModel::createInterpreter() {
    tflite::ops::builtin::BuiltinOpResolver resolver;
    tflite::InterpreterBuilder builder(*model, resolver);
    builder(&interpreter);
    if (interpreter == nullptr) {
        return ErrCode::INIT_INTERPRETER;
    }
    return ErrCode::OK;
}

void TFLiteModel::setDefaultOptions() {
    interpreter->SetAllowFp16PrecisionForFp32(true);
}

ErrCode TFLiteModel::allocateTensors() {
    if (interpreter->AllocateTensors() != kTfLiteOk) {
        return ErrCode::INIT_ALLOCATE;
    }
    return ErrCode::OK;
}

TFLiteModel::~TFLiteModel() {
    release();
}

ErrCode TFLiteModel::load(bool useGPU) {
    if (ErrCode errCode = createInterpreter()) {
        return errCode;
    }

    if (useGPU) {
        auto options = TfLiteGpuDelegateOptionsV2Default();
        gpuDelegate = TfLiteGpuDelegateV2Create(&options);
        if (gpuDelegate == nullptr) {
            return ErrCode::INIT_GPU;
        }
        if (interpreter->ModifyGraphWithDelegate(gpuDelegate) != kTfLiteOk) {
            if (gpuDelegate) {
                TfLiteGpuDelegateV2Delete(gpuDelegate);
                gpuDelegate = nullptr;
            }
            return ErrCode::INIT_GPU;
        }
    }

    setDefaultOptions();

    if (ErrCode errCode = allocateTensors()) {
        return errCode;
    }

    return ErrCode::OK;
}
