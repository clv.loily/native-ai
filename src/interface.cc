#include <memory>
#include <keypoint.h>
#include <opencv2/imgproc.hpp>
#include "interface.h"
#include "unet.h"
#include "keypoint.h"
#include "detcroppose.h"

static std::shared_ptr<UnetModel> model;

static cv::Mat flipInput(cv::Mat inputImage, bool vFlipInput, bool hFlipInput)
{
    cv::Mat image = inputImage;
    if (vFlipInput || hFlipInput)
    {
        int flipCode = 0;
        if (vFlipInput && hFlipInput)
            flipCode = -1;
        else if (vFlipInput)
            flipCode = 0;
        else
            flipCode = 1;

        image = inputImage.clone();
        cv::flip(image, image, flipCode);
    }
    return image;
}

int initUnetModelFromPath(const char *modelPath, bool useGPU) {
    model = std::make_shared<UnetModel>();
    ErrCode errCode = model->loadModelFromPath(modelPath, useGPU);
    if (errCode != ErrCode::OK) {
        model.reset();
    }
    return errCode;
}


int initUnetModelFromBuffer(const void *buffer, size_t bufferSize, bool useGPU) {
    model = std::make_shared<UnetModel>();
    return model->loadModelFromBuffer(buffer, bufferSize, useGPU);
}

void releaseUnetModel() {
    if (model) {
        model->release();
    }
}

void extractAlpha(const void *rgbaData, size_t width, size_t height, size_t numInChannels,
                  uint8_t *outData, size_t numOutChannels, bool vFlipInput, bool hFlipInput) {
    cv::Mat image((int) height, (int) width, CV_8UC(numInChannels), const_cast<void *>(rgbaData));
    image = flipInput(image, vFlipInput, hFlipInput);
    cv::Mat outputImage = model->extractAlpha(image, numOutChannels);
    memcpy(outData, outputImage.reshape(1, 1).data, outputImage.elemSize() * outputImage.total());
}

////////////////////////////

static std::shared_ptr<DetCropPose> keypointModel;

int initKeypointModelFromBuffer(const void *yoloBuffer, size_t yoloBufferSize,
                                const void *keypointBuffer, size_t keypointBufferSize, bool useGPU) {
    ErrCode errCode;
    auto yoloModel = std::make_shared<YoloV5Model>();
    errCode = yoloModel->loadModelFromBuffer(yoloBuffer, yoloBufferSize, useGPU);
    if (errCode != ErrCode::OK) {
        yoloModel.reset();
        return errCode;
    }

    auto kpModel = std::make_shared<KeypointModel>();
    errCode = kpModel->loadModelFromBuffer(keypointBuffer, keypointBufferSize, useGPU);
    if (errCode != ErrCode::OK) {
        yoloModel.reset();
        kpModel.reset();
        return errCode;
    }

    keypointModel = std::make_shared<DetCropPose>(yoloModel, kpModel);
    return ErrCode::OK;
}

int initKeypointModelFromPath(const char *yoloModelPath, const char *keypointModelPath, bool useGPU) {
    ErrCode errCode;
    auto yoloModel = std::make_shared<YoloV5Model>();
    errCode = yoloModel->loadModelFromPath(yoloModelPath, useGPU);
    if (errCode != ErrCode::OK) {
        yoloModel.reset();
        return errCode;
    }

    auto kpModel = std::make_shared<KeypointModel>();
    errCode = kpModel->loadModelFromPath(keypointModelPath, useGPU);
    if (errCode != ErrCode::OK) {
        yoloModel.reset();
        kpModel.reset();
        return errCode;
    }

    keypointModel = std::make_shared<DetCropPose>(yoloModel, kpModel);
    return ErrCode::OK;
}

void releaseKeypointModel() {
    keypointModel->release();
    keypointModel = nullptr;
}

size_t
detectKeypoint(void const* rgbaData, size_t width, size_t height, size_t numInChannels,
               void* outBuffer, size_t bufferSize, bool vFlipInput, bool hFlipInput) {
    cv::Mat image((int) height, (int) width, CV_8UC(numInChannels), const_cast<void *>(rgbaData));
    if (numInChannels == 4) {
        cv::cvtColor(image, image, cv::COLOR_RGBA2RGB);
    }
    image = flipInput(image, vFlipInput, hFlipInput);

    auto&& boxKeypoint = keypointModel->detectKeypoints(image, nullptr);

    std::vector<float> out;
    out.push_back(static_cast<float>(boxKeypoint.size()));
    for (const auto &box : boxKeypoint) {
        out.push_back(static_cast<float>(box.size()));
        for (const auto &keypoint: box) {
            out.push_back(keypoint.x);
            out.push_back(keypoint.y);
        }
    }

    size_t transferSize = out.size() * sizeof(out[0]);
    if (transferSize > bufferSize) {
        return 0;
    }

    std::copy(out.begin(), out.end(), static_cast<float*>(outBuffer));
    return transferSize;
}