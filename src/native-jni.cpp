//
// Created by VinhLoiIT on 5/4/2022.
//

#include <jni.h>
#include <string>
#include "interface.h"

std::string jstring2string(JNIEnv *env, jstring jStr) {
    if (!jStr)
        return "";

    const jclass stringClass = env->GetObjectClass(jStr);
    const jmethodID getBytes = env->GetMethodID(stringClass, "getBytes", "(Ljava/lang/String;)[B");
    const jbyteArray stringJbytes = (jbyteArray) env->CallObjectMethod(jStr, getBytes,
                                                                       env->NewStringUTF("UTF-8"));

    size_t length = (size_t) env->GetArrayLength(stringJbytes);
    jbyte *pBytes = env->GetByteArrayElements(stringJbytes, NULL);

    std::string ret = std::string((char *) pBytes, length);
    env->ReleaseByteArrayElements(stringJbytes, pBytes, JNI_ABORT);

    env->DeleteLocalRef(stringJbytes);
    env->DeleteLocalRef(stringClass);
    return ret;
}

extern "C"
JNIEXPORT void JNICALL
Java_org_opencv_NativeLib_releaseUnetModel(JNIEnv *env, jobject thiz) {
    releaseUnetModel();
}

extern "C"
JNIEXPORT void JNICALL
Java_org_opencv_NativeLib_initModel(JNIEnv *env, jobject thiz, jstring model_path) {
    std::string modelPath = jstring2string(env, model_path);
    initUnetModelFromPath(modelPath.c_str());
}


extern "C"
JNIEXPORT void JNICALL
Java_org_opencv_NativeLib_extractAlpha(JNIEnv *env, jobject thiz, jbyteArray rgba_data, jint width,
                                       jint height, jbyteArray out_data) {
    jbyte *inBufferPtr = env->GetByteArrayElements(rgba_data, NULL);
    jsize inBufferSize = env->GetArrayLength(rgba_data);
    jsize outBufferSize = env->GetArrayLength(out_data);
    assert(inBufferSize == (width * height * 4));
    assert(outBufferSize == (width * height * 4));

    uint8_t *outBuffer = new uint8_t[outBufferSize];
    extractAlpha((uint8_t *) inBufferPtr, width, height, 4, outBuffer, 4);
    env->SetByteArrayRegion(out_data, 0, outBufferSize, (jbyte *) outBuffer);
    env->ReleaseByteArrayElements(rgba_data, inBufferPtr, NULL);
    delete[] outBuffer;
}

extern "C"
JNIEXPORT void JNICALL
Java_org_opencv_NativeLib_initUnetModelFromBuffer(JNIEnv *env, jobject thiz, jbyteArray buffer) {
    jbyte *inBufferPtr = env->GetByteArrayElements(buffer, NULL);
    jsize inBufferSize = env->GetArrayLength(buffer);
    initUnetModelFromBuffer((const void *) inBufferPtr, inBufferSize);
//    env->ReleaseByteArrayElements(buffer, inBufferPtr, NULL);
}

extern "C"
JNIEXPORT void JNICALL
Java_org_opencv_NativeLib_initKeypointModelFromBuffer(JNIEnv *env, jobject thiz,
                                                      jbyteArray yoloBuffer, jbyteArray kpBuffer) {
    jbyte *yoloPtr = env->GetByteArrayElements(yoloBuffer, NULL);
    jsize yoloBufferSize = env->GetArrayLength(yoloBuffer);
    jbyte *kpPtr = env->GetByteArrayElements(kpBuffer, NULL);
    jsize kpBufferSize = env->GetArrayLength(kpBuffer);

    initKeypointModelFromBuffer((const void *) yoloPtr, yoloBufferSize, (const void *) kpPtr,
                                kpBufferSize);
}

extern "C"
JNIEXPORT void JNICALL
Java_org_opencv_NativeLib_releaseKeypointModel(JNIEnv *env, jobject thiz) {
    releaseKeypointModel();
    // TODO: delete ptr in JNI as well
}

extern "C"
JNIEXPORT int JNICALL
Java_org_opencv_NativeLib_detectKeypoint(JNIEnv *env, jobject thiz, jbyteArray rgbaData,
                                         jint width, jint height, jint numInChannels,
                                         jbyteArray outBuffer, jint bufferSize) {
    const void *inputPtr = env->GetByteArrayElements(rgbaData, NULL);
    auto buffer = new int8_t[bufferSize];
    int numBytes = detectKeypoint(inputPtr, width, height, numInChannels, (void*)buffer, bufferSize);
    env->SetByteArrayRegion(outBuffer, 0, numBytes, buffer);
    delete[] buffer;
    return 0;
}