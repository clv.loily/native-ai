#include "opencv2/imgproc.hpp"
#include "keypoint.h"

std::vector <cv::Point2f> KeypointModel::detectKeypoint(const cv::Mat& inputImage) const {
    cv::Mat modelInputImage = preprocessing(inputImage);

    auto inputTensor = interpreter->input_tensor(INPUT_MODEL_TENSOR_INDEX);
    uint32_t totalInputBytes = modelInputImage.total() * modelInputImage.elemSize();

    auto inputBuffer = inputTensor->data.f;
    memcpy(inputBuffer, modelInputImage.reshape(1, 1).data, totalInputBytes);

    interpreter->Invoke();

    auto outputTensor = interpreter->output_tensor(OUTPUT_MODEL_TENSOR_INDEX);
    float *outputArray = outputTensor->data.f;

    std::vector<cv::Point2f> outKeypoint;
    for (size_t i = 0; i < NUM_KEYPOINT * 2; i+=2) {
        float x = outputArray[i] * (float)inputImage.cols;
        float y = outputArray[i + 1] * (float)inputImage.rows;
        outKeypoint.emplace_back(x, y);
    }
    return outKeypoint;
}

cv::Mat KeypointModel::preprocessing(const cv::Mat& image) {
    cv::Mat resized;
    cv::resize(image, resized, cv::Size(INPUT_MODEL_WIDTH, INPUT_MODEL_HEIGHT));
    cv::Mat out;
    resized.convertTo(out, CV_32F, 1 / INPUT_MODEL_STD, -INPUT_MODEL_MEAN);
    return out;
}
