
#include "unet.h"
#include <iostream>
#include "logger.h"
#include <sstream>
#include <fstream>
#include <cstdint>
#include "tflite_model.h"

#include "opencv2/core.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"


constexpr const size_t IN_MODEL_HEIGHT = 128;
constexpr const size_t IN_MODEL_WIDTH = 128;
constexpr const size_t OUT_MODEL_HEIGHT = 128;
constexpr const size_t OUT_MODEL_WIDTH = 128;


static std::shared_ptr<TFLiteModel> model;


void resizeNearestNeighbor(uint32_t *inData, uint32_t width, uint32_t height, uint32_t *outData,
                           uint32_t newWidth, uint32_t newHeight) {
    int x2, y2;
    int whereToPut = 0;
    for (int y = 0; y < newHeight; ++y) {
        for (int x = 0; x < newWidth; ++x) {
            x2 = x * width / newWidth;
            if (x2 < 0)
                x2 = 0;
            else if (x2 >= width)
                x2 = width - 1;
            y2 = y * height / newHeight;
            if (y2 < 0)
                y2 = 0;
            else if (y2 >= height)
                y2 = height - 1;
            outData[whereToPut++] = inData[(y2 * width) + x2];
        }
    }
}
//
//static cv::Mat arrayToMat(const void *rgbaData, size_t width, size_t height, size_t inChannels) {
//    cv::Mat image((int)height, (int)width, CV_8UC(inChannels), const_cast<void *>(rgbaData));
//    cv::Mat resized;
//    cv::resize(image, resized, cv::Size(IN_MODEL_WIDTH, IN_MODEL_HEIGHT), 0.0, 0.0,
//               cv::InterpolationFlags::INTER_LINEAR);
//    if (inChannels == 4) {
//        cv::cvtColor(resized, resized, cv::COLOR_RGBA2RGB);
//    }
//    cv::Mat floatImage;
//    resized.convertTo(floatImage, CV_32F, 1 / 255.0f);
//    return floatImage;
//}


static cv::Mat softmax(cv::Mat inputMat, size_t classIndex, double scale = 1.0, int dtype = -1) {
    std::vector<cv::Mat> outArrays;
    cv::split(inputMat, outArrays);

    cv::Mat softmax = cv::Mat::zeros(outArrays[0].size(), CV_32FC1);
    for (auto &arr : outArrays) {
        cv::Mat expArr;
        cv::exp(arr, expArr);
        softmax += expArr;
    }

    cv::Mat expClass;
    cv::exp(outArrays[classIndex], expClass);

    cv::Mat result;
    cv::divide(expClass, softmax, result, scale, dtype);
    return result;
}

//
//void extractAlpha(const void *rgbaData, size_t width, size_t height, size_t numInChannels,
//                  uint8_t *outData, size_t numOutChannels) {
//    cv::Mat inputImage = arrayToMat(rgbaData, width, height, numInChannels);
//
//    auto inputTensor = interpreter->input_tensor(0);
//    uint32_t totalInputBytes = inputImage.total() * inputImage.elemSize();
//    assert(totalInputBytes == inputTensor->bytes);
//
//    auto inputBuffer = inputTensor->data.f;
//    memcpy(inputBuffer, inputImage.reshape(1, 1).data, totalInputBytes);
//
//    interpreter->Invoke();
//
//    auto outputTensor = interpreter->output_tensor(0);
//
//    cv::Mat outputImage(OUT_MODEL_HEIGHT, OUT_MODEL_WIDTH, CV_32FC2);
//    uint32_t totalOutputBytes = outputImage.elemSize() * outputImage.total();
//    memcpy(outputImage.reshape(1, 1).data, outputTensor->data.f, totalOutputBytes);
//
//    outputImage = softmax(outputImage, DOG_INDEX, 255.0, CV_8U);
//    cv::resize(outputImage, outputImage, cv::Size(width, height), 0.0, 0.0, cv::INTER_LINEAR);
//
//    std::vector<cv::Mat> outChannels;
//    for (size_t i = 0; i < numOutChannels; i++) {
//        outChannels.push_back(outputImage);
//    }
//    cv::merge(outChannels, outputImage);
//
//    memcpy(outData, outputImage.reshape(1, 1).data, outputImage.elemSize() * outputImage.total());
//}
//
//
//void applyMask(const uint8_t *rgbaData, size_t numInChannels, const uint8_t *maskData,
//               size_t numMaskChannels, size_t width, size_t height,
//               uint8_t *outData) {
//    cv::Mat image(height, width, CV_8UC(numInChannels), (void *) rgbaData);
//    cv::Mat mask(height, width, CV_8UC(numMaskChannels), (void *) maskData);
//    image.convertTo(image, CV_32F);
//    mask.convertTo(mask, CV_32F, 1 / 255.0f);
//
//    std::vector<cv::Mat> inChannels;
//    cv::split(image, inChannels);
//    std::vector<cv::Mat> maskChannels;
//    cv::split(mask, maskChannels);
//
//    if (inChannels.size() != numInChannels || maskChannels.size() != numMaskChannels ||
//        (maskChannels.size() > 1 && maskChannels.size() != inChannels.size())) {
//        return;
//    }
//
//    std::vector<cv::Mat> outChannels;
//    if (inChannels.size() == maskChannels.size()) {
//        for (size_t i = 0; i < inChannels.size(); i++) {
//            outChannels.push_back(inChannels[i].mul(maskChannels[i]));
//        }
//    } else {
//        for (size_t i = 0; i < inChannels.size(); i++) {
//            outChannels.push_back(inChannels[i].mul(maskChannels[0]));
//        }
//    }
//    cv::Mat dst(image.size(), image.type());
//    cv::merge(outChannels, dst);
//    dst.convertTo(dst, CV_8U);
//    memcpy(outData, dst.data, dst.total() * dst.elemSize());
//}

cv::Mat UnetModel::extractAlpha(const cv::Mat &image, size_t numOutChannels) const {

    cv::Mat modelInputImage = preprocessing(image);

    auto inputTensor = interpreter->input_tensor(0);
    uint32_t totalInputBytes = modelInputImage.total() * modelInputImage.elemSize();
    assert(totalInputBytes == inputTensor->bytes);

    auto inputBuffer = inputTensor->data.f;
    memcpy(inputBuffer, modelInputImage.reshape(1, 1).data, totalInputBytes);

    interpreter->Invoke();

    auto outputTensor = interpreter->output_tensor(0);

    cv::Mat outputImage(OUT_MODEL_HEIGHT, OUT_MODEL_WIDTH, CV_32FC2);
    uint32_t totalOutputBytes = outputImage.elemSize() * outputImage.total();
    memcpy(outputImage.reshape(1, 1).data, outputTensor->data.f, totalOutputBytes);

    outputImage = postprocessing(outputImage, cv::Size(image.cols, image.rows), numOutChannels);
    return outputImage;
}

cv::Mat UnetModel::preprocessing(const cv::Mat &image) {
    cv::Mat resized;
    cv::resize(image, resized, cv::Size(IN_MODEL_WIDTH, IN_MODEL_HEIGHT), 0.0, 0.0,
               cv::InterpolationFlags::INTER_LINEAR);
    if (resized.channels() == 4) {
        cv::cvtColor(resized, resized, cv::COLOR_RGBA2RGB);
    }
    cv::Mat floatImage;
    resized.convertTo(floatImage, CV_32F, 1 / 255.0f);
    return floatImage;
}

cv::Mat UnetModel::postprocessing(const cv::Mat &modelOutputImage, cv::Size outputSize, size_t numOutChannels) {
    cv::Mat outputImage = softmax(modelOutputImage, DOG_INDEX, 255.0, CV_8U);
    cv::resize(outputImage, outputImage, outputSize, 0.0, 0.0, cv::INTER_LINEAR);

    std::vector<cv::Mat> outChannels;
    for (size_t i = 0; i < numOutChannels; i++) {
        outChannels.push_back(outputImage);
    }
    cv::merge(outChannels, outputImage);
    return outputImage;
}
